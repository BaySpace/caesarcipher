#!/usr/bin/python

#Not too happy about this random library of characters but it was an easy way to have something to rely on. 
#In theory this could also be scrambled or read from a file to increase the level of encryption so that's pretty cool
cipherLib = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'y', 'z', 'æ', 'ø', 'å', ',', '.', '?', '(', ')', '-', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '!', ' ']

def userInput():
    mode = str(input("Encrypt or decrypt? (type either): ")).lower() #Ensuring mode is recognized in main()
    userString = str(input("Input message: "))
    userKey = int(input("Input key: "))

    return mode, userString, userKey

def encrypt(string, key): #Encrypt the input string using the input key
    cipher = string.lower() #Converting to all lower case
    encryptedString = ''
    for i in range(len(cipher)):
        char = cipher[i] #Determining the character of our string at current iteration and assigning to variable
        shiftedChar = (cipherLib.index(char) + key) % len(cipherLib) #Shifting the index by +/- and using the modulus operator to prevent going out of index range
        encryptedString += cipherLib[shiftedChar] #Creating our string from each shifted character
    return encryptedString

#Decrypt function is identical to encrypt() except we just subtract the key from cipherLib.index(char) rather than adding to it
def decrypt(string, key):
    cipher = string.lower()
    encryptedString = '' 
    for i in range(len(cipher)):
        char = cipher[i] 
        shiftedChar = (cipherLib.index(char) - key) % len(cipherLib) 
        encryptedString += cipherLib[shiftedChar]
    return encryptedString


def main():
    mode, string, key = userInput()
    if mode == 'encrypt':
        print(encrypt(string, key))
    elif mode == 'decrypt':
        print(decrypt(string, key))
    else:
        print("Something went wrong, try again.")
main()
